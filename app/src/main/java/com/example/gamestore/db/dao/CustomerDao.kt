package com.example.gamestore.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.gamestore.db.Db
import com.example.gamestore.model.Customer

@Dao
abstract class CustomerDao {
    companion object {
        val db: CustomerDao by lazy { Db.instance.customerDao() }
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertCustomer(customer: Customer)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertCustomers(customers: List<Customer>) : List<Long>

    @Query("SELECT COUNT (*) FROM customer")
    abstract fun getAllCustomerCount(): Int

    @Query("UPDATE customer SET firstName = :firstName, lastName = :lastName, phoneNumber = :phoneNumber, address = :address WHERE email=:email")
    abstract fun updateCustomer(email: String, firstName: String, lastName: String, phoneNumber: String, address: String)

    @Query("SELECT * FROM customer WHERE email=:email")
    abstract fun getCustomerByEmail(email: String): LiveData<Customer>

    @Query("SELECT 1 FROM customer WHERE email=:email")
    abstract fun isCustomerExist(email: String): Int

    @Query("SELECT 1 FROM customer WHERE email=:email AND password=:password")
    abstract fun isCustomerExist(email: String, password: String): Int
}