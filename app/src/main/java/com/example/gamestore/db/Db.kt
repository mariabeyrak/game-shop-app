package com.example.gamestore.db

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import com.example.gamestore.GameStoreApp.Companion.app
import com.example.gamestore.db.dao.CustomerDao
import com.example.gamestore.db.dao.OrderDao
import com.example.gamestore.model.Customer
import com.example.gamestore.model.Order

@Database(entities = [Customer::class, Order::class],
    version = 1)
abstract class Db : RoomDatabase() {

    companion object {
        private const val DB_NAME = "game_store_db"

        val instance: Db by lazy {
            Room.databaseBuilder(app, Db::class.java, DB_NAME)
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .addMigrations(*MIGRATIONS)
                .build()
        }
    }

    abstract fun orderDao(): OrderDao

    abstract fun customerDao(): CustomerDao

}

val MIGRATIONS = arrayOf<Migration>()