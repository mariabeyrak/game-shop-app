package com.example.gamestore.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.gamestore.db.Db
import com.example.gamestore.model.Order

@Dao
abstract class OrderDao {
    companion object {
        val db: OrderDao by lazy { Db.instance.orderDao() }
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertOrder(order: Order)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertOrders(orders: List<Order>) : List<Long>

    @Query("SELECT * FROM customerOrder WHERE customerEmail=:email")
    abstract fun getCustomerOrders(email: String): LiveData<List<Order>>
}