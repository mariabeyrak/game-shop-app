package com.example.gamestore.ui.customer.edit

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.gamestore.db.dao.CustomerDao
import com.example.gamestore.model.Customer
import com.example.gamestore.prefs.UserPrefs
import com.example.gamestore.ui.base.BaseViewModel

class CustomerEditViewModel : BaseViewModel() {
    val currentAccount: LiveData<Customer> = CustomerDao.db.getCustomerByEmail(UserPrefs.getCurrentUser())
    val firstName: MutableLiveData<String> = MutableLiveData()
    val lastName: MutableLiveData<String> = MutableLiveData()
    val phoneNumber: MutableLiveData<String> = MutableLiveData()
    val address: MutableLiveData<String> = MutableLiveData()

    val onSave: MutableLiveData<Unit> by lazy { MutableLiveData<Unit>() }

    fun onSaveClick() {
        CustomerDao.db.updateCustomer(
            currentAccount.value?.email ?: "",
            firstName.value ?: "",
            lastName.value ?: "",
            phoneNumber.value ?: "",
            address.value ?: ""
        )
        onSave.postValue(Unit)
    }
}