package com.example.gamestore.ui.register

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.gamestore.R
import com.example.gamestore.databinding.ActivityRegisterBinding
import com.example.gamestore.ui.base.BaseVMActivity
import com.example.gamestore.ui.main.MainActivity
import com.example.gamestore.ui.signin.SignInActivity

class RegisterActivity : BaseVMActivity<RegisterViewModel>(){

    companion object {
        fun getIntent(context: Context) = Intent(context, RegisterActivity::class.java)
    }

    private val binding: ActivityRegisterBinding by lazy { DataBindingUtil.setContentView<ActivityRegisterBinding>(this, R.layout.activity_register) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.apply {
            viewModel = this@RegisterActivity.viewModel
            setLifecycleOwner(this@RegisterActivity)
            executePendingBindings()
        }

        viewModel.onHasAccount.observe(this, Observer {
            startActivity(SignInActivity.getIntent(this))
        })

        viewModel.onRegister.observe(this, Observer {
            startActivity(MainActivity.getIntent(this))
            finish()
        })

        viewModel.showAlert.observe(this, Observer { text: String ->
            AlertDialog.Builder(this)
                .setMessage(text)
                .setPositiveButton("Ok", null)
                .show()
        })
    }

    override fun provideViewModel(): RegisterViewModel = ViewModelProviders.of(this).get(RegisterViewModel::class.java)
}
