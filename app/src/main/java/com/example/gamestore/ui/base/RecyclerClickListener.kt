package com.example.gamestore.ui.base

open class RecyclerClickListener<in M> {

    open fun onClick(pos: Int, model: M?) {}

    open fun onLongClick(pos: Int, model: M?) {}

}