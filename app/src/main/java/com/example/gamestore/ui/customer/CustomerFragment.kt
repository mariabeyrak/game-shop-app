package com.example.gamestore.ui.customer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.gamestore.R
import com.example.gamestore.databinding.FragmentCustomerBinding
import com.example.gamestore.ui.base.BaseVMFragment
import com.example.gamestore.ui.customer.edit.CustomerEditActivity

class CustomerFragment : BaseVMFragment<CustomerViewModel>() {

    private lateinit var binding: FragmentCustomerBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
        DataBindingUtil.inflate<FragmentCustomerBinding>(inflater, R.layout.fragment_customer, container, false)
            .also { binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            viewModel = this@CustomerFragment.viewModel
            setLifecycleOwner(this@CustomerFragment)
            executePendingBindings()
        }

        viewModel.onEdit.observe(this, Observer {
            context?.let { ctx -> startActivity(CustomerEditActivity.getIntent(ctx)) }
        })

        viewModel.onExit.observe(this, Observer {
            this@CustomerFragment.requireActivity().finish()
        })

    }

    override fun provideViewModel(): CustomerViewModel = ViewModelProviders.of(this).get(CustomerViewModel::class.java)
}
