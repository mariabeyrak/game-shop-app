package com.example.gamestore.ui.signin

import androidx.lifecycle.MutableLiveData
import com.example.gamestore.db.dao.CustomerDao
import com.example.gamestore.model.Customer
import com.example.gamestore.prefs.UserPrefs
import com.example.gamestore.ui.base.BaseViewModel

class SignInViewModel : BaseViewModel() {
    val email: MutableLiveData<String> by lazy { MutableLiveData<String>() }
    val password: MutableLiveData<String> by lazy { MutableLiveData<String>() }

    val onSignIn: MutableLiveData<Unit> by lazy { MutableLiveData<Unit>() }
    val onNoAccount: MutableLiveData<Unit> by lazy { MutableLiveData<Unit>() }
    val showAlert: MutableLiveData<String> by lazy { MutableLiveData<String>() }

    fun checkIfRegistered(){
        if (UserPrefs.getCurrentUser() != ""){
            onSignIn.postValue(Unit)
        }
    }

    fun onSignInClick(){
        if (CustomerDao.db.isCustomerExist(email.value ?: "", password.value ?: "") == 1){
            UserPrefs.saveCurrentUser(email.value ?: "")
            onSignIn.postValue(Unit)
        } else {
            showAlert.postValue("Incorrect email or password")
        }
    }

    fun onNoAccountClick(){
        onNoAccount.postValue(Unit)
    }
}