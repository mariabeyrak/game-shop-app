package com.example.gamestore.ui.main

import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.gamestore.R
import com.example.gamestore.databinding.ActivityMainBinding
import com.example.gamestore.ui.base.BaseVMActivity
import com.example.gamestore.ui.customer.CustomerFragment
import com.example.gamestore.ui.orders.OrdersFragment
import com.example.gamestore.util.ClickThrottler
import com.example.gamestore.util.bindInteger
import com.example.gamestore.util.replaceFragment
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseVMActivity<MainViewModel>() {

    private val navigationBottomClickThrottler = ClickThrottler(bindInteger(android.R.integer.config_mediumAnimTime).toLong())
    private var selectedTabId = Tabs.CUSTOMER.itemId

    private val touchListener = View.OnTouchListener { v, event ->
        if (event.action == MotionEvent.ACTION_DOWN)
            navigationBottomClickThrottler.offer { setCurrentFragment((v as BottomNavigationItemView).id) }
        true
    }

    companion object {
        fun getIntent(context: Context) = Intent(context, MainActivity::class.java)
    }

    private val binding: ActivityMainBinding by lazy {
        DataBindingUtil.setContentView<ActivityMainBinding>(
            this,
            R.layout.activity_main
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.apply {
            viewModel = this@MainActivity.viewModel
            setLifecycleOwner(this@MainActivity)
            executePendingBindings()
        }

        viewModel.visibleTabs.observe(this, Observer { tabs: List<Tabs> ->
            navigationView.menu.clear()

            tabs.forEach { tab ->
                navigationView.menu.add(0, tab.itemId, tab.order, tab.titleInBottomNavBar).setIcon(tab.icon)
            }

            navigationView.run {
                children.forEach {
                    (it as? ViewGroup)?.children?.forEach { view ->
                        view.setOnTouchListener(touchListener)
                    }
                }

                selectedItemId = selectedTabId
                setCurrentFragment(selectedItemId)
            }
        })

    }

    private fun setCurrentFragment(itemId: Int) {
        refreshFragment(binding.navigationView.selectedItemId, itemId)
        binding.navigationView.selectedItemId = itemId
        selectedTabId = itemId
    }

    private fun refreshFragment(prevItemId: Int = binding.navigationView.selectedItemId,
                                itemId: Int = binding.navigationView.selectedItemId) {
        val fragment: Fragment = when (itemId) {
            Tabs.CUSTOMER.itemId -> CustomerFragment()
            Tabs.ORDERS.itemId -> OrdersFragment()
            else -> return
        }

        val prevPos: Int = getBottomNavigationViewPos(prevItemId)
        val currPos: Int = getBottomNavigationViewPos(itemId)

        replaceFragment(R.id.container, fragment) {
            when {
                currPos > prevPos -> it.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                currPos < prevPos -> it.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)
                else -> it.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
            }
        }
    }

    private fun getBottomNavigationViewPos(itemId: Int): Int =
        viewModel.visibleTabs.value?.indexOfFirst { it.itemId == itemId } ?: 0


    override fun provideViewModel(): MainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
}
