package com.example.gamestore.ui.orders

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.gamestore.R
import com.example.gamestore.databinding.FragmentOrdersBinding
import com.example.gamestore.model.Order
import com.example.gamestore.ui.base.BaseVMFragment
import com.example.gamestore.ui.base.RecyclerClickListener
import kotlinx.android.synthetic.main.fragment_orders.*

class OrdersFragment : BaseVMFragment<OrdersViewModel>() {

    private lateinit var binding: FragmentOrdersBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
        DataBindingUtil.inflate<FragmentOrdersBinding>(inflater, R.layout.fragment_orders, container, false)
            .also { binding = it }.root

    private val adapter: OrderRecyclerAdapter by lazy { OrderRecyclerAdapter() }
    private val decorator: DividerItemDecoration by lazy { DividerItemDecoration(this@OrdersFragment.requireContext(), LinearLayoutManager.VERTICAL) }

    private val clickListener = object : RecyclerClickListener<Order>() {
        override fun onClick(pos: Int, model: Order?) {
            super.onClick(pos, model)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            viewModel = this@OrdersFragment.viewModel
            setLifecycleOwner(this@OrdersFragment)
            executePendingBindings()
        }

        recycler.layoutManager = LinearLayoutManager(this@OrdersFragment.requireContext())
        recycler.addItemDecoration(decorator)
        recycler.adapter = adapter

        viewModel.models.observe(this, Observer { models ->
            adapter.setModels(models)
        })

        adapter.setClickListener(clickListener)
    }

    override fun provideViewModel(): OrdersViewModel = ViewModelProviders.of(this).get(OrdersViewModel::class.java)
}
