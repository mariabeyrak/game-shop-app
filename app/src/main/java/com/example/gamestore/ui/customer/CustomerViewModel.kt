package com.example.gamestore.ui.customer

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.gamestore.db.dao.CustomerDao
import com.example.gamestore.model.Customer
import com.example.gamestore.prefs.UserPrefs
import com.example.gamestore.ui.base.BaseViewModel
import com.example.gamestore.util.map

class CustomerViewModel : BaseViewModel() {

    val currentAccount: LiveData<Customer> = CustomerDao.db.getCustomerByEmail(UserPrefs.getCurrentUser())
    val email: LiveData<String> = currentAccount.map { it.email }
    val firstName: LiveData<String> = currentAccount.map { it.firstName.takeIf { it.isNotBlank() } ?: "-" }
    val lastName: LiveData<String> = currentAccount.map { it.lastName.takeIf { it.isNotBlank() } ?: "-" }
    val phoneNumber: LiveData<String> = currentAccount.map { it.phoneNumber.takeIf { it.isNotBlank() } ?: "-" }
    val address: LiveData<String> = currentAccount.map { it.address.takeIf { it.isNotBlank() } ?: "-" }

    val onEdit: MutableLiveData<Unit> by lazy { MutableLiveData<Unit>() }
    val onExit: MutableLiveData<Unit> by lazy { MutableLiveData<Unit>() }

    fun onEditClick() = onEdit.postValue(Unit)
    fun onExitClick() {
        UserPrefs.saveCurrentUser("")
        onExit.postValue(Unit)
    }

}