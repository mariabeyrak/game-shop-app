package com.example.gamestore.ui.customer.edit

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.gamestore.R
import com.example.gamestore.databinding.ActivityCustomerEditBinding
import com.example.gamestore.ui.base.BaseVMActivity
import com.example.gamestore.util.set
import kotlinx.android.synthetic.main.activity_customer_edit.*

class CustomerEditActivity : BaseVMActivity<CustomerEditViewModel>(){

    companion object {
        fun getIntent(context: Context) = Intent(context, CustomerEditActivity::class.java)
    }

    private val binding: ActivityCustomerEditBinding by lazy { DataBindingUtil.setContentView<ActivityCustomerEditBinding>(this, R.layout.activity_customer_edit) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.apply {
            viewModel = this@CustomerEditActivity.viewModel
            setLifecycleOwner(this@CustomerEditActivity)
            executePendingBindings()
        }

        setSupportActionBar(toolbar)

        viewModel.currentAccount.observe(this, Observer {
            viewModel.firstName.set(it.firstName.takeIf { it.isNotBlank() } ?: "-" )
            viewModel.lastName.set(it.lastName.takeIf { it.isNotBlank() } ?: "-" )
            viewModel.phoneNumber.set(it.phoneNumber.takeIf { it.isNotBlank() } ?: "-" )
            viewModel.address.set(it.address.takeIf { it.isNotBlank() } ?: "-" )
        })

        viewModel.onSave.observe(this, Observer {
            finish()
        })

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when (item?.itemId) {
        android.R.id.home -> onBackPressed().let { true }
        else -> super.onOptionsItemSelected(item)
    }

    override fun provideViewModel(): CustomerEditViewModel = ViewModelProviders.of(this).get(CustomerEditViewModel::class.java)
}
