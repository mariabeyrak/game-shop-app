package com.example.gamestore.ui.orders

import androidx.lifecycle.LiveData
import com.example.gamestore.db.dao.OrderDao
import com.example.gamestore.model.Order
import com.example.gamestore.prefs.UserPrefs
import com.example.gamestore.ui.base.BaseViewModel

class OrdersViewModel : BaseViewModel() {
    val models: LiveData<List<Order>> = OrderDao.db.getCustomerOrders(UserPrefs.getCurrentUser())
}