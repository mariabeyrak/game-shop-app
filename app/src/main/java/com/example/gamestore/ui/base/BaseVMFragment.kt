package com.example.gamestore.ui.base

import android.os.Bundle
import androidx.fragment.app.Fragment

abstract class BaseVMFragment<T : BaseViewModel> : Fragment() {

    protected lateinit var viewModel: T

    abstract fun provideViewModel(): T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = provideViewModel()
    }
}