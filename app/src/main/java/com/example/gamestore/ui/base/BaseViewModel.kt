package com.example.gamestore.ui.base

import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.ViewModel
import com.example.gamestore.util.ClickThrottler
import com.example.gamestore.util.JobComposite

abstract class BaseViewModel : ViewModel(), Observable {

    val jobComposite by lazy { JobComposite() }

    @Transient
    private var callbacks: PropertyChangeRegistry? = null

    val clickThrottler = ClickThrottler(550)

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        synchronized(this) {
            if (callbacks == null) {
                callbacks = PropertyChangeRegistry()
            }
        }
        callbacks?.add(callback)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        synchronized(this) {
            if (callbacks == null) {
                return
            }
        }
        callbacks?.remove(callback)
    }

    /**
     * Notifies listeners that a specific property has changed. The getter for the property
     * that changes should be marked with [Bindable] to generate a field in
     * `BR` to be used as `fieldId`.
     *
     * @param fieldId The generated BR id for the Bindable field.
     */
    private fun notifyPropertyChanged(fieldId: Int) {
        synchronized(this) {
            if (callbacks == null) {
                return
            }
        }
        callbacks?.notifyCallbacks(this, fieldId, null)
    }

    fun notifyPropertyChanged(vararg fields: Int) {
        fields.forEach { notifyPropertyChanged(it) }
    }

    override fun onCleared() {
        super.onCleared()
        jobComposite.cancel()
    }
}