package com.example.gamestore.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.gamestore.ui.base.BaseViewModel
import com.example.gamestore.util.JobBuilder
import com.example.gamestore.util.startWith

class MainViewModel : BaseViewModel() {
    val visibleTabs: LiveData<List<Tabs>> = MutableLiveData<List<Tabs>>().startWith(
        mutableListOf(Tabs.CUSTOMER, Tabs.ORDERS).sortedBy { it.order }
    )
}