package com.example.gamestore.ui.main

import com.example.gamestore.R
import com.example.gamestore.util.bindString

enum class Tabs(val titleInBottomNavBar: String, val icon: Int, val itemId: Int, val order: Int) {
    CUSTOMER(bindString(R.string.customer), R.drawable.ic_account, R.id.action_customer, 1),
    ORDERS(bindString(R.string.orders), R.drawable.ic_list, R.id.action_orders, 2)
}