package com.example.gamestore.ui.register

import androidx.lifecycle.MutableLiveData
import com.example.gamestore.db.dao.CustomerDao
import com.example.gamestore.model.Customer
import com.example.gamestore.prefs.UserPrefs
import com.example.gamestore.ui.base.BaseViewModel

class RegisterViewModel : BaseViewModel() {
    val email: MutableLiveData<String> by lazy { MutableLiveData<String>() }
    val password: MutableLiveData<String> by lazy { MutableLiveData<String>() }

    val onRegister: MutableLiveData<Unit> by lazy { MutableLiveData<Unit>() }
    val onHasAccount: MutableLiveData<Unit> by lazy { MutableLiveData<Unit>() }
    val showAlert: MutableLiveData<String> by lazy { MutableLiveData<String>() }

    fun onRegisterClick(){
        if (CustomerDao.db.isCustomerExist(email.value ?: "") == 1){
            showAlert.postValue("Customer already exist")
        } else {
            CustomerDao.db.insertCustomer(
                Customer(
                email.value ?: "",
                password.value ?: "",
                "",
                "",
                    "",
                    ""
            ))
            UserPrefs.saveCurrentUser(email.value ?: "")
            onRegister.postValue(Unit)
        }
    }

    fun onHasAccountClick(){
        onHasAccount.postValue(Unit)
    }
}