package com.example.gamestore.ui.orders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.gamestore.R
import com.example.gamestore.model.Order
import com.example.gamestore.ui.base.BaseRecyclerAdapter
import com.example.gamestore.ui.base.BaseViewHolder
import com.example.gamestore.ui.base.RecyclerClickListener
import kotlinx.android.synthetic.main.item_order.view.*
import java.text.SimpleDateFormat
import java.util.*

class OrderRecyclerAdapter : BaseRecyclerAdapter<OrderViewHolder, Order>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderViewHolder =
        OrderViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_order, parent, false))

}

class OrderViewHolder(val view: View) : BaseViewHolder<Order>(view) {

    override fun bind(model: Order) {
        view.description?.text = model.description
        view.date?.text = SimpleDateFormat("MMM d, yyyy k:mm",
            Locale.getDefault()).format(Date(model.dateOfOrder))
        itemView.setOnClickListener {
            (clickListenerWeak.get() as RecyclerClickListener<Order>).onClick(adapterPosition, model)
        }
    }
}