package com.example.gamestore.prefs

import android.content.Context
import android.content.SharedPreferences
import com.example.gamestore.GameStoreApp

private const val KEY_CURRENT_EMAIL = "key.email"

object UserPrefs {
    private val prefs: SharedPreferences by lazy { GameStoreApp.app.getSharedPreferences("userPrefs", Context.MODE_PRIVATE) }

    fun saveCurrentUser(currentEmail: String) = prefs.edit().putString(KEY_CURRENT_EMAIL, currentEmail).apply()
    fun getCurrentUser(): String = prefs.getString(KEY_CURRENT_EMAIL, "")
}