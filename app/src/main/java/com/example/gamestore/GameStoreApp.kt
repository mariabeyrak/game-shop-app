package com.example.gamestore

import android.app.Application
import com.example.gamestore.db.dao.CustomerDao
import com.example.gamestore.db.dao.OrderDao
import com.example.gamestore.model.Customer
import com.example.gamestore.model.Order

open class GameStoreApp : Application() {

    companion object {
        lateinit var app: GameStoreApp
    }

    override fun onCreate() {
        super.onCreate()
        app = this
        populateDb()
    }

    private fun populateDb() {
        if (CustomerDao.db.getAllCustomerCount() == 0) {
            CustomerDao.db.insertCustomers(
                listOf(
                    Customer("test@gmail.com", "Qwerty1", "Test", "user", "", "")
                )
            )

            OrderDao.db.insertOrders(
                listOf(
                    Order("test@gmail.com", 1561068501000, "Some games"),
                    Order("test@gmail.com", 1561068501000, "Some test games"),
                    Order("test@gmail.com", 1561068501000, "More games")
                )
            )
        }
    }
}