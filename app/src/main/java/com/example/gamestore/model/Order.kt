package com.example.gamestore.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.gamestore.util.DiffComparable

@Entity(tableName = "customerOrder")
data class Order(
    val customerEmail: String,
    val dateOfOrder: Long,
    val description: String
) : DiffComparable {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    override fun areItemsTheSame(item: Any): Boolean =
        (item as? Order)?.let {
            it.customerEmail == customerEmail
                    && it.dateOfOrder == dateOfOrder && it.description == description
        } ?: false

    override fun areContentsTheSame(item: Any): Boolean =
        (item as? Order)?.let {
            it.customerEmail == customerEmail
                    && it.dateOfOrder == dateOfOrder && it.description == description
        } ?: false
}