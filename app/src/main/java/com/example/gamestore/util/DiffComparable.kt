package com.example.gamestore.util

interface DiffComparable {
    fun areItemsTheSame(item: Any): Boolean

    fun areContentsTheSame(item: Any): Boolean
}