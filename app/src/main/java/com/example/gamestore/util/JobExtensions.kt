package com.example.gamestore.util

import kotlinx.coroutines.*

/**
 * Extension which allows you to cancel Job with composite object while you build your coroutine
 * @param composite object which allows to cancel all at once
 */
fun Job.cancelWith(composite: JobComposite): Job {
    composite.add(this)
    return this
}

/**
 * JobBuilder which allows to build coroutine job with error handling without adding a try{}catch{}
 * or pre-coroutine block
 */
class JobBuilder(private val task: suspend CoroutineScope.() -> Unit) {
    private var context: CoroutineDispatcher = Dispatchers.Default
    private var errorHandler: CoroutineExceptionHandler = CoroutineExceptionHandler { _, e -> kotlinx.coroutines.GlobalScope.launch(
        Dispatchers.Main) { throw e } }
    private var cancelComposite: JobComposite = JobComposite()

    fun withContext(context: CoroutineDispatcher): JobBuilder {
        this.context = context
        return this
    }

    fun handleError(errHandler: (Throwable) -> Unit): JobBuilder {
        this.errorHandler = CoroutineExceptionHandler { _, e -> kotlinx.coroutines.GlobalScope.launch(Dispatchers.Main) { errHandler(e) } }
        return this
    }

    fun cancelWith(composite: JobComposite): JobBuilder {
        this.cancelComposite = composite
        return this
    }

    fun run() {
        GlobalScope.launch(context + errorHandler, block = {
            task()
        }).cancelWith(cancelComposite)
    }

    fun run(composite: JobComposite) {
        cancelWith(composite).run()
    }
}