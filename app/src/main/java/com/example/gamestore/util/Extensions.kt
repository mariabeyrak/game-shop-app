package com.example.gamestore.util

import android.os.Looper
import androidx.annotation.IntegerRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.transaction
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.gamestore.GameStoreApp

fun bindString(@StringRes res: Int): String = GameStoreApp.app.getString(res)

fun bindString(@StringRes res: Int, vararg input: Any): String = GameStoreApp.app.getString(res, *input)

fun bindInteger(@IntegerRes res: Int): Int = GameStoreApp.app.resources.getInteger(res)

fun LiveData<Boolean>.get(default: Boolean = false) = this.value ?: default

fun LiveData<Int>.get(default: Int = 0) = this.value ?: default

fun <T> MutableLiveData<T>.set(value: T?) {
    if (Looper.myLooper() == Looper.getMainLooper()) setValue(value)
    else postValue(value)
}

fun <T> MutableLiveData<T>.default(value: T): MutableLiveData<T> = this.also { set(value) }

fun <T1, T2> LiveData<T1>.flatMap(function: (T1) -> LiveData<T2>): LiveData<T2> = Transformations.switchMap(this, function)

fun <T1, T2> LiveData<T1>.map(function: (T1) -> T2): LiveData<T2> = Transformations.map(this, function)

fun <T> LiveData<T>.startWith(first: T): LiveData<T> = MediatorLiveData<T>().apply {
    set(first)
    addSource(this@startWith) { set(it) }
}

fun <T1, T2> LiveData<T1>.zip(source2: LiveData<T2>): LiveData<Pair<T1, T2>> = MediatorLiveData<Pair<T1, T2>>().apply {
    addSource(this@zip) { data -> value = data to (source2.value ?: return@addSource) }
    addSource(source2) { data -> value = (this@zip.value ?: return@addSource) to data }
}

inline fun AppCompatActivity.replaceFragment(containerId: Int, fragment: Fragment, changes: (FragmentTransaction) -> Unit = {}) {
    supportFragmentManager.apply {
        transaction {
            changes(this)
            replace(containerId, fragment)
        }
        executePendingTransactions()
    }
}