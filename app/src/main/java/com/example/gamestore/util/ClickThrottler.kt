package com.example.gamestore.util

class ClickThrottler(val delay: Long = DEFAULT_DELAY) {

    companion object {

        const val DEFAULT_DELAY = 550L
    }

    private var lastOffer = 0L

    fun offer(action: () -> Unit) {
        val now = System.currentTimeMillis()
        if (now - lastOffer > delay) {
            action()
            lastOffer = now
        }
    }
}